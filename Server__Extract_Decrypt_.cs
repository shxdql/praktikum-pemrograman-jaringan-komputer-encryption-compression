﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading.Tasks;


namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpListener listener = new TcpListener(IPAddress.Any, 8080);
            listener.Start();

            Console.WriteLine("Server Started!");

            while(true)
            {
                TcpClient client = listener.AcceptTcpClient();

                Console.WriteLine("Connected to Client");

                StreamReader reader = new StreamReader(client.GetStream());

                string pass = reader.ReadLine();
                string fileName = reader.ReadLine();
                string cmdfileSize = reader.ReadLine();
                string cmdFileName = reader.ReadLine();

                int length = Convert.ToInt32(cmdfileSize);
                byte[] buffer = new byte[length];
                int rcv = 0;
                int read = 0;
                int size = 1024;
                int remain = 0;

                while(rcv < length)
                {
                    remain = length - rcv;
                    if(remain < size)
                    {
                        size = remain;
                    }
                    read = client.GetStream().Read(buffer, rcv, size);
                    rcv = read;
                }

                using(FileStream fs=new FileStream(Path.GetFileName(cmdFileName), FileMode.Create))
                {
                    fs.Write(buffer, 0, buffer.Length);
                    fs.Flush();
                    fs.Close();
                }

                Console.WriteLine("File Received and Saved in " + Environment.CurrentDirectory);

                int uni = 92;
                char ch = (char)uni;
                string txt = ch.ToString();

                string zipPath = Environment.CurrentDirectory + txt + fileName;
                string extractPath = @"E:\Ihsan\Visual Studio\Jarkom\Server\Server\bin\Debug";
                string encPath = @"E:\Ihsan\Visual Studio\Jarkom\Jarkom\bin\Debug";

                ZipFile.ExtractToDirectory(zipPath, extractPath);
                Console.WriteLine("\nFile Extracted and Saved in " + extractPath);

                DecryptdFile(extractPath, encPath, pass);
                Console.Write("\nFile Decrypted with pass " + pass);


                Console.WriteLine("\nFile Decrypted with Pass : " + pass);

            }
        }

        public static byte[] Decrypt(byte[] bytesToBeDecrypted,byte[] pass)
        {
            byte[] decrypted = null;
            byte[] salt = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using(MemoryStream ms=new MemoryStream())
            {
                using (RijndaelManaged rijn = new RijndaelManaged())
                {
                    rijn.KeySize = 256;
                    rijn.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(pass, salt, 1000);
                    rijn.Key = key.GetBytes(rijn.KeySize / 8);
                    rijn.IV = key.GetBytes(rijn.BlockSize / 8);
                    rijn.Mode = CipherMode.CBC;

                    using(var cs=new CryptoStream(ms,rijn.CreateDecryptor(),CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decrypted = ms.ToArray();
                }
            }
            return decrypted;
        }

        public static void DecryptdFile(string encryptedFile,string file,string pass)
        {
            byte[] bytesToBeDecrypted = File.ReadAllBytes(encryptedFile);
            byte[] passbytes = Encoding.UTF8.GetBytes(pass);
            passbytes = SHA256.Create().ComputeHash(passbytes);

            byte[] bytesDecrypted = Decrypt(bytesToBeDecrypted, passbytes);

            File.WriteAllBytes(file, bytesDecrypted);
        }
    }
}
