# Praktikum Pemrograman Jaringan Komputer Encryption Compression

4210181017
Muhammad Shodiqul Ihsan

1. Client Encrypts File.
2. Client Compresses File.
3. Client Sends Zip File.
4. Server Receives Zip File.
5. Server Extracts Zip File.
6. Server Decrypts Zip File.